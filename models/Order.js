const mongoose = require("mongoose");

let orderSchema = new mongoose.Schema({

	userId: {
		type: String
	},
	totalAmount: {
		type: Number,
		required: [true, "totalAmount is required."]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	products: [

	
		{
			productId: {
				type: String,
				required: [true, "Product ID is required."]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required."]
			}
		}

	]

})

module.exports = mongoose.model("Order",orderSchema);