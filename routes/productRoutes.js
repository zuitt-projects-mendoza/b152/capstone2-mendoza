const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;


router.post('/',verify,verifyAdmin,productControllers.createProduct);

router.get('/',productControllers.getAllProducts);

router.get('/getActiveProducts',productControllers.getActiveProducts);

router.get('/getSingleProduct/:id',productControllers.getSingleProduct);

router.put('/update/:id',verify,verifyAdmin,productControllers.updateProduct);

router.put('/archive/:id',verify,verifyAdmin,productControllers.archiveProduct);

router.put('/activate/:id',verify,verifyAdmin,productControllers.activateProduct);

module.exports = router;