const Order = require("../models/Order");

const Product = require("../models/Product");

const User = require("../models/User");

module.exports.order = (req,res) => {

	console.log(req.user.id)
	console.log(req.body.products)

	if(req.user.isAdmin) {
		return res.send("Action Forbidden")
	}

		let newOrder = new Order({

			userId: req.user.id,
			totalAmount: req.body.totalAmount,
			products: req.body.products

		})
		
		var orderId = newOrder.id
		console.log(orderId)

		let products = req.body.products;

		products.forEach(function(foundProduct){

		let isProductUpdated = Product.findById(foundProduct.productId).then(product => {

				let newOrders = {

					orderId: orderId,
					quantity: foundProduct.quantity

				}

			product.orders.push(newOrders);


			return product.save().then(result => res.send(result)).catch(err => res.send(err))

			})

		})

		return newOrder.save()
		.then(result => res.send({message: "Order Successful."}))
		.catch(err => res.send(err));

}

module.exports.getAllOrders = (req,res) => {

	Order.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}

module.exports.getUserOrders = (req,res) => {

	Order.find({userid:req.user.id})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}