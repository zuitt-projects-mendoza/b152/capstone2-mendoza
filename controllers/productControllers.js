const Product = require("../models/Product");

module.exports.createProduct = (req,res) => {

	console.log(req.body);

	let newProduct = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})

	newProduct.save()
	.then(result => res.send(result)) 
	.catch(error => res.send(error))


}

module.exports.getAllProducts = (req,res) => {

	Product.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}

module.exports.getActiveProducts = (req,res) => {

	Product.find({isActive:true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.getSingleProduct = (req,res) => {

	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

module.exports.updateProduct = (req,res) => {

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.archiveProduct = (req,res) => {

	let updates = {

		isActive: false

	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.activateProduct = (req,res) => {

	let updates = {

		isActive: true

	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

